numpy>=1.19
scipy>=1.6
matplotlib
plotly>=4
pandas
jinja2
beautifulsoup4
nibabel
hlsvdpropy
fslpy>=3.9.0
pillow
spec2nii>=0.3.0
dill
dash
nifti-mrs>=0.1.4
